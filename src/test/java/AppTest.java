import com.example.Application;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class AppTest {

    @Test
    public void testApp() {
        Application myApp = new Application();
        String result = myApp.getStatus();
        assertEquals("OK", result);
    }
}
